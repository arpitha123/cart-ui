import reducer from "./reducer";
import {combineReducers} from "redux";

//combineReducers({reducer,reducer1})
const rootReducer = combineReducers({reducer})

export default rootReducer;
