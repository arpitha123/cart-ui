import initVal from "../utils/initVal";
const reducer = (state = initVal, action) => {
	switch (action.type) {
		case "NAME":
			state = {
				...state,
				name: action.payload
			}
			break;
		case "EditProduct":
			state = {
				...state,
				editProduct: action.payload
			}
			break;
		case "SaveForLater":
			state = {
				...state,
				saveForLater: [...state.saveForLater, action.payload]
			}
			break;
		case "TotalAmt":
			state = {
				...state,
				subTotal: action.payload
			}
			break;
		case "CartData":
			state = {
				...state,
				data: action.payload
			}

	}
	return state;
}
export default reducer;
