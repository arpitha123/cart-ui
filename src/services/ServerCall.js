import axios from "axios";

class ServerCall {
	static fnGetReq(url){
		return axios.get(url);
	}
	static fnPostReq(url,data){
		return axios.post(url,data);
	}
}
export default ServerCall;
