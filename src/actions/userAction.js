import ServerCall from "../services/ServerCall";
//import data from '';

const userAction = (dispatch) => {
    ServerCall.fnGetReq("../../utils/data.json").then(
        (res)=>{
            dispatch({
                type : "USERS",
                payload : res
            })
        },
        (err)=>{
            dispatch({
                type : "USERS",
                payload : []
            })
        }
    )
}
export default userAction;