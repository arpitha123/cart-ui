import ServerCall from "../services/ServerCall";


const photosAction = () =>{
    return (dispatch,getState)=>{
        ServerCall.fnGetReq("http://jsonplaceholder.typicode.com/photos").
        then((res)=>{
            dispatch({
                type : "PHOTOS",
                payload : res.data
            })
        }).catch((res)=>{
            dispatch({
                type : "PHOTOS",
                payload : []
            })
        })
      
    }
}

export default photosAction;