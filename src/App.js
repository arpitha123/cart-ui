import React from 'react';
import './App.css';
import Header from "./components/Header/index";
import Footer from "./components/Footer/index";
import Cart from "./components/Cart/index";
import "bootstrap/dist/css/bootstrap.css";


function App() {
  return (
    <div className="App container-fluid">
      <Header />
      <Cart />
      <Footer />
    </div>
  );
}

export default App;
