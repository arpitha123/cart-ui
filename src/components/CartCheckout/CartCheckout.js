import React    from "react";
import template from "./CartCheckout.jsx";

class CartCheckout extends React.Component {
  render() {
    return template.call(this);
  }
}

export default CartCheckout;
