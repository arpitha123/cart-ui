import "./CartCheckout.css";
import React from "react";
import HelpSection from "../HelpSection/index";
import CheckoutSection from "../CheckoutSection/index";

function template() {
  return (
    <div className="cart-checkout row">
      <HelpSection />
      <CheckoutSection />
    </div>
  );
};

export default template;
