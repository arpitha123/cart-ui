import React from "react";
import template from "./ProductDetail.jsx";
import { connect } from 'react-redux'
class ProductDetail extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isModal: false
    }
    this.fnCalculate = this.fnCalculate.bind(this);
    // console.log(props)
  }
  componentDidMount() {
    this.fnCalculate(this.props.data);
  }
  fnRemove(item) {
    let cartData = [];
    Object.assign(cartData, this.props.data);
    let delItemIndex = cartData.findIndex((obj) => {
      return obj.p_id == item.p_id
    });
    cartData.splice(delItemIndex, 1)
    this.props.dispatch({
      type: 'CartData',
      payload: cartData
    })
    this.fnCalculate(cartData);
  }

  fnEdit(item) {
    this.props.dispatch({
      type: 'EditProduct',
      payload: item
    })
    this.setState({
      isModal: true
    })
  }

  fnSaveForLater(item) {
    let cartData = [];
    Object.assign(cartData, this.props.data);
    let delItemIndex = cartData.findIndex((obj) => {
      return obj.p_id == item.p_id
    });
    cartData.splice(delItemIndex, 1)
    this.props.dispatch({
      type: 'CartData',
      payload: cartData
    })
    this.props.dispatch({
      type: 'SaveForLater',
      payload: item
    })
    this.fnCalculate(cartData);
  }

  fnClose() {
    this.setState({
      isModal: false
    })
    setTimeout(() => {
      this.fnCalculate(this.props.data);
    }, 100);
  }
  fnCalculate(cartData) {
    debugger;
    let total = cartData.reduce((amt, obj) => {
      amt = amt + Number(obj.p_quantity * obj.p_price);
      return amt;
    }, 0);
    this.props.dispatch({
      type: 'TotalAmt',
      payload: total
    })
  }
  render() {
    return template.call(this);
  }
}
const mps = (state) => {
  return {
    data: state.reducer.data
  }
}
const mdp = (dispatch) => {
  return {
    dispatch
  }
}
export default connect(mps, mdp)(ProductDetail)
