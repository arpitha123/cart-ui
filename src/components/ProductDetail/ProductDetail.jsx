import "./ProductDetail.css";
import React from "react";
import T1 from "../../utils/img/T1.jpg";
import T2 from "../../utils/img/T2.jpg";
import T3 from "../../utils/img/T3.jpg";
import T4 from "../../utils/img/T4.jpg";
import EditModal from "../EditModal/index";
import ModalEdit from '../../ModalEdit/ModalEdit'

function template() {
  console.log("***", this.props.item)
  const imgObj = {
    "/images/T1.jpg": T1,
    "/images/T2.jpg": T2,
    "/images/T3.jpg": T3,
    "/images/T4.jpg": T4

  }
  const { p_name, p_style, p_selected_color, p_image,p_available_options } = this.props.item;
  debugger;
  return (
    <div className="product-detail ">
      <div className="row">
        <div className="col-md-3 ">
          <img src={imgObj[p_image]} />
        </div>
        <div className="col-md-9">
          <p className='text-left'><b>{p_name}</b></p>
          <p className='text-left'>Style #: {p_style}</p>
          <p className='text-left'>Color: {p_selected_color.name}</p>
          <br />
          <p className='text-left'>
            <a onClick={this.fnEdit.bind(this, this.props.item)} className="alert-link">EDIT</a> |
            <a onClick={this.fnRemove.bind(this, this.props.item)} className="alert-link">X REMOVE</a> |
            <a onClick={this.fnSaveForLater.bind(this, this.props.item)} className="alert-link">Save for Later</a>
          </p>
        </div>
      </div>
      {this.state.isModal && <ModalEdit fnClose={this.fnClose.bind(this)} />}

    </div>
  );
};

export default template;
