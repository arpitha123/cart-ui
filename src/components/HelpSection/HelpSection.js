import React    from "react";
import template from "./HelpSection.jsx";

class HelpSection extends React.Component {
  render() {
    return template.call(this);
  }
}

export default HelpSection;
