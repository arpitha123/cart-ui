import "./HelpSection.css";
import React from "react";

function template() {
  return (
    <div className="help-section col-md-4 text-left">
        <p><b>Need Help or have questions?</b></p>
        <p>Call Customer Service at </p>
        <p>1-800-555-555</p>
        <p><a href="#" className="alert-link">Chat with one of our stylist</a></p>
        <p><a href="#" className="alert-link">See retur or exchange policy</a></p>
      </div>    
  );
};

export default template;
