import "./Header.css";
import React from "react";


function template() {
  return (
    <div className="header text-left">
      <p className="cart-header">YOUR SHOPPING CART</p>
      <p className="cart-sub-header">If the cart is empty then we shall again add back the products for you.</p>
    </div>
  );
};

export default template;
