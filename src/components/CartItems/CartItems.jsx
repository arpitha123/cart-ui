import "./CartItems.css";
import React from "react";
// import data from '../../utils/data.json';
import ProductDetail from '../ProductDetail/index';

function template() {
  const {data}=this.props;
  return (
    <div className="cart-items">
      <table className="table">
        <thead>
          <tr >

            <th className="text-left">{data.length} ITEMS</th>
            <th >SIZE</th>
            <th >QTY</th>
            <th >PRICE</th>
          </tr>

        </thead>
        <tbody>
          {
            data.map((item, index) => {
              console.log(item)
              return <tr key={"tr" + index} >
                <td ><ProductDetail item={item} /></td>
                <td >{item.p_selected_size.code.toUpperCase()}</td>
                <td >{item.p_quantity}</td>
                <td >{item.p_price}</td>
              </tr>
            })
          }
        </tbody>
      </table>
      <hr style={{
        color: '#ebebeb',
        backgroundColor: '#ebebeb',
        height: 5,
        borderColor: '#ebebeb'
      }} />
    </div>
  );
};

export default template;
