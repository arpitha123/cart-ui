import React    from "react";
import template from "./CartItems.jsx";
import {connect} from 'react-redux'

class CartItems extends React.Component {
  componentDidMount(){
    
  }
  render() {
    return template.call(this);
  }
}
const mps=(state)=>{
   return {
     data:state.reducer.data
   }
}
const mdp=(dispatch)=>{
  return {
    dispatch
  }
}
export default connect(mps,mdp)(CartItems)