import React    from "react";
import template from "./CheckoutSection.jsx";
import {connect} from 'react-redux'
class CheckoutSection extends React.Component {
 
  render() {
    return template.call(this);
  }
}
const mps = (state) => {
  return {
    subTotal:state.reducer.subTotal
  }
}

export default connect(mps)(CheckoutSection)

