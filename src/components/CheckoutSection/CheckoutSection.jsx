import "./CheckoutSection.css";
import React from "react";
import lock from "../../utils/img/lock.png";
function template() {
  return (
    <div className="checkout-section col-md-8 text-left">
      <div className="checkout-table">
        <div>
          <p className="text-left">ENTER PROMOTION CODE OR GIFT CARD</p>
          <p className="text-right"><input type="text" /><input type="button" value="Apply" /></p>
        </div>
        <hr />
        <div>
          < p className="text-left">SUB TOTAL</p>
          <p className="text-right">{this.props.subTotal} $</p>
        </div>
        <div>
          <p className="text-left">PROMOTION CODE AJ10 applied</p>
          <p className="text-right">$5.00</p>

        </div>
        <div>
          <p className="text-left">ESTIMATED SHIPPING*<br />You quality for free shipping because your order is over $50</p>
          <p className="text-right">Free</p>
        </div>
        <hr />
        <div>
          <p className="text-left">ESTIMATED Total*<br />Tax will be applied during checkout</p>
          <p className="text-right">$53.10</p>
        </div>
        <hr style={{
          color: '#ebebeb',
          backgroundColor: '#ebebeb',
          height: 5,
          borderColor: '#ebebeb'
        }} />        
        <div className="continue-shopping">
          <p className="text-left"></p><p className="text-right">Continue Shopping <input type="button"  value="CHECKOUT" /></p>
        </div>
        <div>
          <p className="text-left"></p>
          <p className="text-right"><img src={lock} />Secure Checkout. Shopping is always safe and secure.</p>
        </div>
      </div>
    </div>
  );
};

export default template;
