import "./EditModal.css";
import React from "react";

function template() {
  const {
    onCloseRequest,
    children,
    classes,
  } = this.props;
  return (
    // <div className="edit-modal">
    //   <div></div>    
    // </div>
    <div className={classes.modalOverlay}>
        <div
          className={classes.modal}
          ref={node => (this.modal = node)}
        >
          <div className={classes.modalContent}>
            {children}
          </div>
        </div>

        <button
          type="button"
          className={classes.closeButton}
          onClick={onCloseRequest}
        />
      </div>
  );
};

export default template;
