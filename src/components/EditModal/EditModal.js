import React    from "react";
import template from "./EditModal.jsx";

class EditModal extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return template.call(this);
  }
}

export default EditModal
