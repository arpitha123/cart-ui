import React    from "react";
import template from "./Cart.jsx";

class Cart extends React.Component {
  render() {
    return template.call(this);
  }
}

export default Cart;
