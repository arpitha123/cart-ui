import "./Cart.css";
import React from "react";
import CartItems from "../CartItems/index";
import CartCheckout from "../CartCheckout/index";

function template() {
  return (
    <div className="cart">
     <CartItems />
     <CartCheckout />
    </div>
  );
};

export default template;
