import "./ModalEdit.css";
import React from "react";
import T1 from "../utils/img/T1.jpg"
import T2 from "../utils/img/T2.jpg";
import T3 from "../utils/img/T3.jpg";
import T4 from "../utils/img/T4.jpg";
const imgObj = {
  "/images/T1.jpg": T1,
  "/images/T2.jpg": T2,
  "/images/T3.jpg": T3,
  "/images/T4.jpg": T4

}
function template() {
  const { p_name, p_image,p_selected_size,p_quantity, p_available_options, p_price, c_currency,p_selected_color } = this.props.editProduct;
  return (
    <div className="modal-edit">
      <div className="mask"></div>
      <div className="content">
        <span className="close-btn" onClick={this.props.fnClose}>x</span>
        <div className="row">
          <div className="col-md-6 m-10">

            <div>{p_name}</div>
            <div className='f-10'>{c_currency}{p_price}</div>
            <p>
              {
                p_available_options.colors && p_available_options.colors.map((clrObj) => {
                  return <div onClick={this.fnGetColor.bind(this, clrObj)} className="clr-div" style={{ background: clrObj.name }}></div>
                })
              }
            </p>
            <p>Color:{this.state.color.name ? this.state.color.name : p_selected_color.name  }</p>
            <p>
              <select ref='sizeRef' >
                {
                  p_available_options.sizes && p_available_options.sizes.map((sizeObj) => {
                    return <option  value={sizeObj.code}  selected={p_selected_size.code==sizeObj.code}>{sizeObj.name}</option>
                  })
                }
              </select>
              <input type='number' min='1' defaultValue={p_quantity} ref='quantityRef'/>
            </p>
            <div className="row">
             <div className='col-sm-12 '>
             <input type='button' className='btn btn-info' value="Edit" onClick={this.fnEdit.bind(this)}/>
             </div>
          
          </div>
          </div>
          <div className="col-md-6 text-center m-20">
            <img src={imgObj[p_image]} />
          </div> 
        
        </div>
       
      </div>
    </div>
  );
};

export default template;
