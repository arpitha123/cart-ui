import React    from "react";
import template from "./ModalEdit.jsx";
import {connect} from 'react-redux'
class ModalEdit extends React.Component {
  constructor(){
    super();
    this.state={
      color:{}
    }
  }
  fnGetColor(clrObj){
    debugger;
   this.setState({
     color:clrObj
   })
  }
  fnEdit(){
    debugger;
    let sizeRef=this.refs.sizeRef;
    let sizeObj={
      "name":sizeRef.innerText,
      "code": sizeRef.value
    }
    let quantity=this.refs.quantityRef.value;

    let editProductInfo={};
    let cartData=[];
    Object.assign(cartData,this.props.cartData);
    Object.assign(editProductInfo,this.props.editProduct);
    editProductInfo.p_selected_color=this.state.color;
    editProductInfo.p_selected_size=sizeObj;
    editProductInfo.p_quantity=quantity;
    let updateItemIndex = cartData.findIndex((obj) => {
      return obj.p_id == editProductInfo.p_id
    });
    cartData.splice(updateItemIndex,1,editProductInfo)
    this.props.dispatch({
      type:'CartData',
      payload:cartData
    })
    this.props.fnClose();
  }
  render() {
    return template.call(this);
  }
}
const mps = (state) => {
  return {
    editProduct: state.reducer.editProduct,
    cartData:state.reducer.data
  }
}
const mdp = (dispatch) => {
  return {
    dispatch
  }
}
export default connect(mps, mdp)(ModalEdit)
